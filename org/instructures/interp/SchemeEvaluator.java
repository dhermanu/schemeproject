package org.instructures.interp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.instructures.interp.Problem.EvaluationError;
import org.instructures.interp.values.*;
import org.instructures.interp.values.CompoundDatum.Pair;
import org.instructures.interp.values.LexemeDatum.SymbolDatum;

public class SchemeEvaluator {
  private final Environment environment;

  public SchemeEvaluator() {
    this.environment = Primitives.newGlobalEnvironment();
  }

  /**
   * Driver for testing the evaluator. All of the files input for evaluation are
   * executed in the same environment, as if they were all concatenated together
   * and evaluated as a single file.
   */
  public static void main(String[] args) {
    SchemeEvaluator evaluator = new SchemeEvaluator();
    for (String filename: args) {
      try (BufferedReader in = new BufferedReader(new FileReader(filename))) {
        DatumParser parser = new DatumParser(in);
        for (;;) {
          try {
            Datum datum = parser.nextDatum();
            if (datum == null) {
              break;
            }
            Value value = evaluator.evaluate(datum);
            if (value.isSpecified()) {
              System.out.printf("%s%n", value.toSyntaxString());
            }
          } catch (EvaluationError e) {
            System.err.printf("%s:%s%n", filename, e.getMessage());
          }
        }
      } catch (Exception e) {
        System.err.printf("%s:%s%n", filename, e.getMessage());
      }
    }
  }

  /**
   * Evaluates the S-Expression specified by the given datum. If the expression
   * does not have a defined value then {@code null} is returned. In the event
   * of a syntax or type error, a Problem will be thrown.
   */
  public Value evaluate(Datum sExpr) throws Problem {
    return evaluate(sExpr, environment);
  }

  private static Value evaluate(Datum sExpr, Environment environment) {
    // YOUR CODE HERE //
    /*DELETE THIS*/return null;/*DELETE THIS*/
    // EXAMPLE FROM CLASS:
    //    if (sExpr.isSymbol()) {
    //      return environment.lookupVariable(sExpr.toString());
    //    } else if (sExpr.isEmptyList()) {
    //      throw Problem.invalidExpression("Application operator must have a procedure");
    //    } else if (sExpr.isPair()) {
    //      return evaluateNonEmptyListForm(sExpr, environment);
    //    } else {
    //      // then just assume it's self-evaluating
    //      return sExpr;
    //    }
  }

  // YOUR CODE HERE //
  // EXAMPLE CODE:
  //  private static class Lambda extends RuntimeValue.Procedure {
  //    private List<String> formals;
  //    private List<Datum> body;
  //    private Environment scope;
  //
  //    public Lambda(List<String> formals, boolean isVarArg, List<Datum> body, Environment scope) {
  //      super(formals.size() - (isVarArg ? 1 : 0), isVarArg);
  //      this.formals = formals;
  //      this.body = body;
  //      this.scope = scope;
  //    }
  //
  //    @Override
  //    public Value checkedApply(LinkedList<Value> args) {
  //      Environment extendedEnvironment = scope.extend(formals, args);
  //      return evaluateSequence(body, extendedEnvironment);
  //    }
  //  }

  private static void assertCondition(boolean condition, String fmt, Object... args) {
    if (!condition) {
      throw Problem.invalidExpression(fmt, args);
    }
  }
}
