package org.instructures.interp;

import static org.instructures.interp.LexicalUtils.*;
import java.io.*;
import java.util.function.Predicate;

public class TokenScanner {
  private final LineNumberReader reader;

  public TokenScanner(Reader in) {
    this.reader = new LineNumberReader(in);
    reader.setLineNumber(1);
  }

  /**
   * Driver for testing the scanner code.
   */
  public static void main(String[] args) {
    for (String filename: args) {
      try (BufferedReader in = new BufferedReader(new FileReader(filename))) {
        TokenScanner scanner = new TokenScanner(in);
        Lexeme token;
        do {
          token = scanner.nextToken();
          System.out.printf("%s:%02d: %s%n", filename, token.getLineNumber(), token);
        } while (token.isNoneOf(TokenType.EOF));
      } catch (Exception e) {
        System.err.printf("TokenScanner: %s%n", e.getMessage());
        e.printStackTrace(System.err);
      }
    }
  }

  public Lexeme nextToken() throws IOException {
    String tr = readNext(1);
    while (isWhitespace(tr) || tr.equals(";")) {
      if (tr.equals(";")) {
        reader.readLine();
      }
      tr = readNext(1);
    }
    if (tr.isEmpty()) {
      return result(TokenType.EOF, "");
    } else if (tr.equals(".")) {
      String lookahead = peekNext(2);
      if (lookahead.equals("..")) {
        match(lookahead);
        return result(TokenType.SYMBOL, "...");
      }
      return result(TokenType.DOT, ".");
    } // YOUR CODE HERE //
    
    else if(tr.equals("#"))
    {
    	String lookahead = peekNext(1);
    	String lexeme = tr + readNext(1);
    	//System.out.println("HELLO WORLD" + lexeme);
    	if(isBoolean(lexeme))
    	{
    		return result(TokenType.BOOLEAN, lexeme);
    	}
    	else if(lexeme.equals("#\\"))
    	{
    	   String maybeName = lexeme + peekNext(LexicalUtils.getMaxCharacterLiteralLength());
    	   String lookaheadAgain = peekNext(1);
    	   for(String characterName: LexicalUtils.getCharacterNames()){
    		   if(maybeName.startsWith(characterName)){
    			   match(characterName.substring(lexeme.length()));
    			   return result(TokenType.CHARACTER, characterName);
    		   }
    	   }
    	   
    	   if(isInitial(lookaheadAgain))
    	   {
    		   String rest = appendRest(lexeme,LexicalUtils::isSubsequent);
    		   return result(TokenType.CHARACTER, rest);
    	   }
    	}
    	
    	else
    	{
    		match(lookahead);
    		return error(lookahead, "Invalid character after \"#\": #%s", lookahead);
    	}
    }
    
    
    else if(isPunctuation(tr)){
    	return result(parsePunctuation(tr), tr); 
    }
    else if(isInitial(tr)){
    	return result(TokenType.SYMBOL, appendRest(tr, LexicalUtils::isSubsequent));
    }
    else if(isDigit(tr)){
    	return result(TokenType.NUMBER, appendRest(tr, LexicalUtils::isDigit));
    }
    
    else if (tr.equals("-") || tr.equals("+")) {
    	String lookahead = peekNext(1);
    	//String lexeme = tr + readNext(1);

    	if(isDigit(lookahead))
    	{
    		return result(TokenType.NUMBER, appendRest(tr,LexicalUtils::isDigit));
    	}
    	else if(tr.equals("-") && lookahead.equals(">"))
    	{
    		return result(TokenType.SYMBOL, appendRest(tr, LexicalUtils::isSubsequent));
    	}
    	return result(TokenType.SYMBOL, tr);
    }
    
    else if (tr.equals("\"")) {
   
        String lexeme = tr; 	
    	tr = readNext(1);
    	
  	    while(isSubsequent(tr) || isPunctuation(tr) || isWhitespace(tr) || tr.equals("\"")
  	    		|| tr.equals("\\"))
    	{
  	    	if(!(tr.equals("\\")||tr.equals("\"")))
        	{
  	    		lexeme +=tr;
  	        	tr = readNext(1);
        	}
  	    	
  	    	if(tr.equals("\""))
        	{    
        		String lookahead = peekNext(1);
        		if(isSubsequent(lookahead))
        		{
        			lexeme +=tr;
                	tr = readNext(1);
        		}
        		
        		else
        		{
        			lexeme +=tr;
        	    	return result(TokenType.STRING, lexeme);
        		}		
        	}
        	
        	else if(tr.equals("\\"))
    		{
        		String lookahead = peekNext(1);
        		String result = tr + lookahead;
        		
                if(isEscapedStringElement(result) || isWhitespace(lookahead))
                {
                	lexeme +=tr;
                	tr = readNext(1);
                }
    			   
                else
                {   
                	return error(result, "Invalid string escape: \"%s\"", result);
                }
    		}
        }
    	
  	    if(tr.isEmpty()){
    		return error(tr, "Undertermined string");
    	}        
    	return result(TokenType.STRING, lexeme);
    	
    }
 
    
    
    // SOME EXAMPLES:
    // ... else if (tr.equals("#")) { ...
    //} else if (tr.equals("\"")) { ...
    //} else if (isPunctuation(tr)) { ...
    //  return result(parsePunctuation(tr), tr);
    //} else if (isInitial(tr)) { ...
    //} else if (tr.equals("-") || tr.equals("+")) { ...
    //} else if (isDigit(tr)) {
    //  return result(TokenType.NUMBER, appendRest(tr, LexicalUtils::isDigit));
    //}
    return error(tr, "Unexpected character: \"%s\"", tr);
  }

  private String appendRest(String lexeme, Predicate<String> byCharPredicate) throws IOException {
    return appendRest(lexeme, byCharPredicate, Integer.MAX_VALUE);
  }

  private String appendRest(String lexeme, Predicate<String> byCharPredicate, int limit) throws IOException {
    String lookahead = peekNext(1);
    while (byCharPredicate.test(lookahead) && limit-- > 0) {
      lexeme += lookahead;
      match(lookahead);
      lookahead = peekNext(1);
    }
    return lexeme;
  }

  // Peeks ahead and returns the next characters to be read. The length of the
  // string returned depends on how many characters are left in the input.
  private String peekNext(int nextCharsCount) throws IOException {
    reader.mark(nextCharsCount + 1);
    String result = readNext(nextCharsCount);
    reader.reset();
    return result;
  }

  // Returns a string containing as many of the given count of characters as
  // possible.
  private String readNext(int nextCharsCount) throws IOException {
    char[] buff = new char[nextCharsCount];
    int actualLength = reader.read(buff, 0, buff.length);
    return (actualLength >= 1) ? new String(buff, 0, actualLength) : "";
  }

  private void match(String charsToSkip) throws IOException {
    reader.skip(charsToSkip.length());
  }

  private Lexeme result(TokenType tokenType, String text) {
    int lineNumber = reader.getLineNumber();
    return new Lexeme(tokenType, text, lineNumber);
  }

  private Lexeme error(String text, String format, Object... args) throws IOException {
    String errorMessage = String.format(format, args);
    int lineNumber = reader.getLineNumber();
    // consume the rest of the line to reduce cascading errors
    reader.readLine();
    return new Lexeme(TokenType.ERROR, text, errorMessage, lineNumber);
  }
}
